import { faker } from '@faker-js/faker';
import { IUser } from './user.interface';
import * as db from '../../db';

export const get = async (id: number): Promise<IUser> => {
  /* Imagine that here we call to the database using something like:
    const data = await db.collection('users).findOne(...) 
  */

  const data = {
    id,
    name: faker.name.fullName(),
    email: faker.internet.email(),
    password: faker.random.word(),
    createdAt: faker.datatype.datetime(),
    updatedAt: faker.datatype.datetime(),
  };
  return data;
};

export const update = async (id: number, data: any): Promise<IUser> => {
  /* Imagine that here we call to the database using something like:
    await db.collection('users').updateOne(...)
  */

  const user = {
    id,
    name: data.name,
    email: data.email,
    password: faker.random.word(),
    createdAt: faker.datatype.datetime(),
    updatedAt: faker.datatype.datetime(),
  };

  return user;
};
