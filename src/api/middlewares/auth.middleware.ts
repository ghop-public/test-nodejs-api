import { NextFunction, Request, Response } from 'express';
import jwt from 'jsonwebtoken';

const authentication = (req: Request, res: Response, next: NextFunction) => {
  const { authorization } = req.headers;

  if (!authorization) {
    return res
      .status(401)
      .json({ status: 401, message: 'MISSING AUTH HEADER' });
  }

  const token = authorization.slice('Bearer'.length).trim();

  /* Imagine that the JWT token is verified here using something like:
    const isVerified = jwt.verify(...) 
  */
  const isVerified = true;

  if (!isVerified) {
    return res.status(401).json({ status: 401, message: 'INVALID TOKEN' });
  }

  return next();
};

export default authentication;
