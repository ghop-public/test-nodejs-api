export interface IStore {
  id: number;
  name: string;
  address: string;
  createdAt: Date;
  updatedAt?: Date;
}

export class Store implements IStore {
  id!: number;
  name!: string;
  address!: string;
  createdAt!: Date;
  updatedAt?: Date;
}
