import mongoDB, { MongoClient } from 'mongodb';

const dbUri = process.env.DB_URI || '';
const dbName = process.env.DB_NAME || '';

export let db: mongoDB.Db;

export const connect = async () => {
  try {
    /* Imagine that here we connect to the database using something like:
      const client = new MongoClient(dbUri);
      await client.connect()
      db = client.db(dbName)
    */

    console.log('Connected to DB');
  } catch (e: any) {
    throw new Error(e.message);
  }
};
